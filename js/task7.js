// Завдання 7
// Доповніть код так, щоб він коректно працював

// const array = ['value', () => 'showValue'];

// // Допишіть код тут

// alert(value); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'


const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value);         // 'value'
alert(showValue());   // 'showValue'